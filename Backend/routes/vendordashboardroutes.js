//create owner dashboard routes
const exp = require('express');
//import bcrypt module
const bcrypt = require('bcrypt');
var vendordashboardroutes = exp.Router();
const checkauthorization = require('../middleware/checkauthorization')
//importing dbconfig file
const initDb = require('../DBconfig').initDb;
const getDb = require('../DBconfig').getDb;
initDb();
//import request handlers
//importing post request for dopay 
vendordashboardroutes.post('/dopay', checkauthorization, (req, res, next) => {
    console.log(req.body);
    dbo = getDb();
    if (req.body == {}) {
        res.json({ "message": "no data from client" });
    }
    else {
        dbo.collection('dopayCollection').insertOne(req.body, (err, success) => {
            if (err) {
                console.log("error in save operation");
                next(err);
            }
            else {
                res.json({ "message": "successfully registered" })
            }
        });
    }
});
//import get request for paymenthistory
vendordashboardroutes.get('/paymentshistory', checkauthorization, (req, res, next) => {
    dbo.collection('dopayCollection').find().toArray((err, data) => {
        if (err) {
            console.log("error in save operation");
            next(err)
        }
        else {
            res.json({ "message": data })
        }
    });
});
//importing get request for whoomtolet
vendordashboardroutes.get('/whoomtolet', checkauthorization, (req, res, next) => {
    dbo = getDb();
    dbo.collection('houseCollection').find().toArray((err, data) => {
        if (err) {
            console.log("error in save operation");
            next(err);
        }
        else {
            res.json({ "message": data })
        }
    });
});
//edit profile using put request
vendordashboardroutes.put('/edit', checkauthorization, (req, res, next) => {
    console.log(req.body);
    bcrypt.hash(req.body.upassword, 5, (err, hashedpassword) => {
        if (err) {
            next(err);
        }
        else {
            //replace plane text password to hashed password
            req.body.upassword = hashedpassword;
            dbo = getDb();
            dbo.collection('vendor').updateOne({ username: { $eq: req.body.username } }, {
                $set: {
                    date: req.body.date, mail: req.body.mail, number: req.body.number,
                    address: req.body.address, upassword: req.body.upassword
                }
            }, (err, success) => {
                if (err) {
                    console.log("error in updating data..");
                    next(err);
                }
                else {
                    res.json({ "message": "updated successfully..." })
                }
            })
        }
    })
})
//intrested house post request
vendordashboardroutes.post('/whoomtolet', checkauthorization, (req, res, next) => {
    console.log(req.body);
    dbo = getDb();
    if (req.body.length == 0) {
        res.json({ "message": "no data from client" });
    }
    else {
        dbo.collection('tolet').insertOne(req.body, (err, success) => {
            if (err) {
                console.log("error in save operation");
                next(err);
            }
            else {
                res.json({ "message": "Request sent successfully" })
            }
        });
    }
});

//import error handling
vendordashboardroutes.use((err, req, res, next) => {
    console.log(err);
})
module.exports = vendordashboardroutes;
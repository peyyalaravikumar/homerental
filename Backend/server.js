//install express module
//import express module
const exp = require('express');
const app = exp();

//install path module
//import path module
const path = require('path');
//importing angular app with server
app.use(exp.static(path.join(__dirname, '../dist/project1/')));
//import bodyparser
const bodyparser = require('body-parser');
//use body parser
app.use(bodyparser.json());
//import user routes
const userroutes = require('./routes/userroutes');
//informing server about routes
app.use('/nav', userroutes);
//import owner routes
const ownerdashboardroutes = require('./routes/ownerdashboardroutes');
//informing server about routes
app.use('/dashboard', ownerdashboardroutes);
//import vendor routes
const vendordashboardroutes = require('./routes/vendordashboardroutes');
//informing server about routes
app.use('/home', vendordashboardroutes);
//import admin routes
const adminroutes = require('./routes/adminroutes');
//informing server about admin routes
app.use('/admin', adminroutes);
//assign port number
app.listen(process.env.PORT || 8080, () => { console.log('server is running..') });
const jwt = require('jsonwebtoken');
const secretkey = "secret";
var checkauthorization = (req, res, next) => {

    //read authorization in req object
    var token = req.headers['authorization'];
    console.log(token)
    //if token is found,check validity
    if (token == undefined) {
        return res.json({ "message": "unauthorized access" })
    }
    else if (token.startsWith('Bearer ')) {
        token = token.slice(7, token.length);
        jwt.verify(token, secretkey, (err, decoded) => {
            if (err) {
                return res.json({ "message": "Invalid access" })
            }
            //forward to next midleware or req handler
            else {
                next();
            }
        })
    }
}
module.exports = checkauthorization;
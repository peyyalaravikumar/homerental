import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  currentuser: any;
  constructor(private hc: HttpClient) { }
  setResponse(data): Observable<any> {
    return this.hc.put('dashboard/viewclient', data)
  }
  deleteClient(data): Observable<any> {
    return this.hc.delete('/dashboard/viewclient', data)
  }
}

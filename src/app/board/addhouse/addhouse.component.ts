import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RegisterService } from 'src/app/register.service';

@Component({
  selector: 'app-addhouse',
  templateUrl: './addhouse.component.html',
  styleUrls: ['./addhouse.component.css']
})
export class AddhouseComponent implements OnInit {
  constructor(private http: HttpClient, private hc: RegisterService) { }

  ngOnInit() {
  }
  addhouse(data) {
    if (data.address == "") {
      alert("address field is mandatory")
    }
    else {
      //console.log(data)
      data.username = this.hc.currentuser[0].username;
      this.http.post('/dashboard/addhouse', data).subscribe((res) => {
        alert(res["message"])
      });
    }
  }
}

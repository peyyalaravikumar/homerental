import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RegisterService } from 'src/app/register.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  constructor(private http: HttpClient, private hc: RegisterService) { }

  ngOnInit() {
  }
  addpay(data) {
    if (data.account == "" || data.ifsc == "") {
      alert("account and ifsc fields are mandatory")
    }
    else {
      //console.log(data)
      data.username = this.hc.currentuser[0].username;
      this.http.post('/dashboard/addpayments', data).subscribe((res) => {
        alert(res["message"])
      });
    }
  }
}

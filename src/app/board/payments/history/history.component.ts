import { Component, OnInit } from '@angular/core';
import { HouseService } from 'src/app/house.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { RegisterService } from 'src/app/register.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  clients: any[] = [];
  constructor(private http: HttpClient, private register: RegisterService) { }
  ngOnInit() {
    this.http.get(`/dashboard/viewpaymenthistory/${this.register.currentuser[0].username}`).subscribe(res => {
      this.clients = res['data']
    })
    // console.log(this.clients)
    console.log(this.register.currentuser[0].username)
  }

}

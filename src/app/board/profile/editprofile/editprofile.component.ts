import { Component, OnInit } from '@angular/core';
import { RegisterService } from 'src/app/register.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.component.html',
  styleUrls: ['./editprofile.component.css']
})
export class EditprofileComponent implements OnInit {
  currentuser: any;
  constructor(private cm: RegisterService, private http: HttpClient, private router: Router) { }

  ngOnInit() {
    this.currentuser = this.cm.currentuser[0]
    console.log(this.cm.currentuser)
  }
  edit(data) {
    if (data.number == "" || data.upaasword == "") {
      alert("mobileno & password are mandatory")
    }
    else {
      //console.log(data);
      this.http.put('dashboard/editprofile', data).subscribe(res => {
        alert(res["message"])

        this.router.navigate(['/dashboard/viewprofile'])
      })
    }
  }
}

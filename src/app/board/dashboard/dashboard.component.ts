import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private d: Router) { }

  ngOnInit() {
  }
  logout() {
    localStorage.removeItem('idToken');
    this.d.navigate(['/nav/carousel']);
  }
}

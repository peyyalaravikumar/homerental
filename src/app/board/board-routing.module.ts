import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NavprofileComponent } from './navprofile/navprofile.component';
import { ViewprofileComponent } from './profile/viewprofile/viewprofile.component';
import { EditprofileComponent } from './profile/editprofile/editprofile.component';
import { AddhouseComponent } from './addhouse/addhouse.component';
import { ViewhouseComponent } from './viewhouse/viewhouse.component';
import { ViewclientComponent } from './viewclient/viewclient.component';
import { PaymentsComponent } from './payments/payments.component';
import { AddComponent } from './payments/add/add.component';
import { ViewComponent } from './payments/view/view.component';
import { HistoryComponent } from './payments/history/history.component';
import { MyrequestsComponent } from './myrequests/myrequests.component';


const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent,
    children: [{
      path: 'profile',
      component: NavprofileComponent
    },
    {
      path: 'viewprofile',
      component: ViewprofileComponent
    },
    {
      path: 'editprofile',
      component: EditprofileComponent
    },
    {
      path: 'addhouse',
      component: AddhouseComponent
    },
    {
      path: 'viewhouse',
      component: ViewhouseComponent
    },
    {
      path: 'viewclient',
      component: ViewclientComponent
    },
    {
      path: 'payments',
      component: PaymentsComponent
    },
    {
      path: 'addpayment',
      component: AddComponent
    },
    {
      path: 'viewpayment',
      component: ViewComponent
    },
    {
      path: 'viewpaymenthistory',
      component: HistoryComponent
    },
    {
      path: 'myrequests',
      component: MyrequestsComponent
    }
    ]

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BoardRoutingModule { }

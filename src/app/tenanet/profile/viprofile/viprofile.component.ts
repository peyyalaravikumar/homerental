import { Component, OnInit } from '@angular/core';
import { RegisterService } from 'src/app/register.service';

@Component({
  selector: 'app-viprofile',
  templateUrl: './viprofile.component.html',
  styleUrls: ['./viprofile.component.css']
})
export class ViprofileComponent implements OnInit {
  currentuser: any;
  constructor(private cm: RegisterService) { }

  ngOnInit() {
    this.currentuser = this.cm.currentuser[0]
    console.log(this.cm.currentuser)
  }

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterService } from 'src/app/register.service';
import { HttpClient } from '@angular/common/http';
import { LoginService } from '../login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: any[];
  constructor(private ds: Router, private rc: HttpClient, private cm: RegisterService) { }

  ngOnInit() {
  }
  method(data) {
    if (data.username == '' || data.password == '' || data.user == '') {
      alert('username,password,usertype are mandatory')
    }
    else {
      this.rc.post('nav/login', data).subscribe((res) => {
        //console.log(res["message"]);
        if (res["message"] == "Invalid owner name") {
          alert("please enter valid owner name");
        }
        else if (res["message"] == "Invalid owner password") {
          alert("please enter valid password");
        }
        else if (res["message"] == "owner success") {
          alert("owner successfully login..")
          localStorage.setItem("idToken", res['token']);
          //console.log(res['token']);
          //console.log(res['userdata'])
          this.cm.currentuser = res['userdata']
          this.ds.navigate(['/dashboard/viewprofile'])
        }

        else if (res["message"] == "Invalid vendor name") {
          alert("please enter valid vendor name");
        }
        else if (res["message"] == "Invalid vendor password") {
          alert("please enter valid password");
        }
        else if (res["message"] == "vendor success") {
          alert("vendor successfully login");
          localStorage.setItem("idToken", res['token']);
          //console.log(res['token']);
          //console.log(res['userdata'])
          this.cm.currentuser = res['userdata']
          this.ds.navigate(['/home/viprofile'])
        }
      })
    }
  }
}

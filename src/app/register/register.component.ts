import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private c: Router, private http: HttpClient) { }

  ngOnInit() {
  }
  Change(data) {
    if (data.username == '' || data.upassword == '' || data.user == '') {
      alert('username,password,usertype are mandatory')
    }
    else {
      //console.log(data)
      this.http.post('/nav/register/', data).subscribe((res) => {
        if (res["message"] == "name is already exist") {
          alert("username is already exists")
        }
        else if (res["message"] == "successfully registered") {
          alert("regstered successfully..")

          this.c.navigate(['/nav/login']);
        }
      })
    }
  }
}